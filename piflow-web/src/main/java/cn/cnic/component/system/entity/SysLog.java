package cn.cnic.component.system.entity;

import cn.cnic.base.BaseHibernateModelUUIDNoCorpAgentId;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.security.Key;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "SYS_LOG")
public class SysLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    private String username;

    private String lastLoginIp;

    private String action;

    private Boolean status;

    private String result;

    private String comment;

    private Date ctrDttm;

    private Date lastUpdateDttm;

    private Boolean enableFlag;



}

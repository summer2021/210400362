SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `crt_dttm` datetime NOT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `enable_flag` bit(1) NOT NULL,
  `last_update_dttm` datetime NOT NULL,
  `last_update_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `version` bigint(20) NULL DEFAULT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(3) NULL DEFAULT NULL,
  `last_login_ip` varchar(63) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`
(
    `id`               int(11) NOT NULL AUTO_INCREMENT,
    `username`         varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
    `last_login_ip`    varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员地址',
    `type`             int(11) NULL DEFAULT NULL COMMENT '操作分类',
    `action`           varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作动作',
    `status`           tinyint(1) NULL DEFAULT NULL COMMENT '操作状态',
    `result`           varchar(127) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作结果，或者成功消息，或者失败消息',
    `comment`          varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '补充信息',
    `crt_dttm`         datetime NULL DEFAULT NULL COMMENT '创建时间',
    `last_update_dttm` datetime NULL DEFAULT NULL COMMENT '更新时间',
    `enable_flag`      bit(1) NULL DEFAULT b'0' COMMENT '逻辑删除',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 95 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

UPDATE `piflow_web_1.0`.`sys_user` SET `crt_dttm` = '2021-08-16 09:38:28', `crt_user` = 'system', `enable_flag` = b'1', `last_update_dttm` = '2021-08-16 09:38:28', `last_update_user` = 'admin', `version` = 0, `age` = NULL, `name` = 'admin', `password` = '$2a$10$Owy5vEZ9vCpNE3FNpYUjze2RGqwoy8RamrzY5j7vKzapJsIjdApxS', `sex` = NULL, `username` = 'admin', `status` = 0, `last_login_ip` = NULL WHERE `id` = 'bef148e608004bd8a72e658fed2f9c9f';

SET FOREIGN_KEY_CHECKS = 1;
